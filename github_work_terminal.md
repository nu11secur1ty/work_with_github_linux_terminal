# Stage the file for commit to your local repository.

```bash
git init
git add .
```

# Adds the file to your local repository and stages it for commit. To unstage a file, use 'git reset HEAD YOUR-FILE'.
- Commit the file that you've staged in your local repository.

```bash
git commit -m "Add existing file"
```
# Add remote
```bash
git remote add origin https://github.com/nu11secur1ty/kernel-4.19.0V.Varbanovski_lp150.12.22_default
```


# Commits the tracked changes and prepares them to be pushed to a remote repository. To remove this commit and modify the file, use 'git reset --soft HEAD~1' and commit and add the file again.

- Push the changes in your local repository to GitHub.

```bash
git push origin your-branch
```
# Pushes the changes in your local repository up to the remote repository you specified as the origin
