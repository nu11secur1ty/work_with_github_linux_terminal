# Create a new repository on the command line
```bash
echo "# curl_diploy" >> README.md
git init
git add README.md
git commit -m "first commit"
git remote add origin https://github.com/nu11secur1ty/curl_diploy.git
git push -u origin master
```
# Or push an existing repository from the command line
```bash
git remote add origin https://github.com/nu11secur1ty/curl_diploy.git
git push -u origin master
```
----------------------------------------

# Adding new file
```bash
touch file.pl
git add file.pl
git commit -m "Comment"
git pull
git push origin master
```
----------------------------------------
# Sync
```bash 
git pull origin master
```
