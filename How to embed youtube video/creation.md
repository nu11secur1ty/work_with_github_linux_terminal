One of the workarounds for this issue with videos is to insert an image of for the video wrapped in a link pointing to the video location. For markdown files images are inserted in the following way:
```git
![alt text](http://example.com/exampl.png)
```
whereas we use the following markup to insert links
```git
[link text](http://example.com/link "title")
```
So, combining these two markups allows us inserting image with link to the markdown
```git
[![alt text](http://example.com/exampl.png)](http://example.com/link "title")
```
Good! Now we know how to have clickable images in our markdown which will redirect us to some link. But how can we do the same trick with youtube video?

