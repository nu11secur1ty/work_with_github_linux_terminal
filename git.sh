#!/usr/bin/bash
# by Vetntsislav Varbanovski @nu11secur1ty
# nu11secur1ty.com

repo_name=$1
    test -z $repo_name && echo "Repo name required: Example: bash git.sh _your_repo_name" 1>&2 && exit 1

# 'nu11secur1ty' - must be your github username
curl -u 'nu11secur1ty' https://api.github.com/user/repos -d "{\"name\":\"$repo_name\"}"

exit 0;
